package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class LeilaoTeste {

    private Leilao leilao;
    private Usuario usuario;
    Lance lance;
    private List<Lance> lancesList;
    Lance lanceRetorno;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        usuario = new Usuario(1, "Carol");
        lance = new Lance(2.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        lance = new Lance(3.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        Lance lance = new Lance(4.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);
        lancesList= leilao.getLances();
    }

    @Test
    public void testarIncluirUmNovoLance(){
        lance = new Lance(6.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        Assertions.assertSame(lance, lanceRetorno);
    }

    @Test
    public void testarIncluirComValidacaoDeLanceMenorQueAnterior(){
        Usuario usuario = new Usuario(1, "Carol");
        lance = new Lance(2.00, usuario);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lance);});
    }

    @Test
    public void testarIncluirComValidacaoDeLanceMaiorQueAnterior(){
        Usuario usuario = new Usuario(1, "Carol");
        lance = new Lance(8.00, usuario);

        Assertions.assertSame(lance, leilao.validarLance(lance));
    }

    @Test
    public void testarIncluirUmLanceJaInclusoNaLista(){
         lance = new Lance(3.00, usuario);

        Assertions.assertTrue(leilao.verificarSeJaExisteLance(lance));
    }

    @Test
    public void testarValidarLanceMenorQueAnterior(){
        Usuario usuario = new Usuario(1, "Carol");
        Lance lance = new Lance(1.00, usuario);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.validarLance(lance);});
    }

    @Test
    public void testarValidarLanceMaiorQueAnterior(){
        Usuario usuario = new Usuario(1, "Carol");
        Lance lance = new Lance(5.00, usuario);

        Assertions.assertSame(lance, leilao.validarLance(lance));
    }
}
