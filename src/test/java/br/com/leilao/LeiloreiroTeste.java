package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class LeiloreiroTeste {

    private Leiloeiro leiloeiro;
    private Usuario usuario;
    private Leilao leilao;
    Lance lance;
    private List<Lance> lances;
    Lance lanceRetorno;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        usuario = new Usuario(1, "Carol");
        lance = new Lance(2.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        lance = new Lance(3.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        lance = new Lance(4.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        usuario = new Usuario(1, "");
        lance = new Lance(5.00, usuario);
        lanceRetorno =leilao.adicionarNovoLance(lance);

        lances= leilao.getLances();

        leiloeiro = new Leiloeiro("Teste", lances);
    }

    @Test
    public void testarRetornoDoValorMaiorLance(){
        Assertions.assertEquals(5.00, leiloeiro.retornarMaiorLance().getValorDoLance());
    }

    @Test
    public void testarRetornoDoNomeMaiorLance(){
        Assertions.assertEquals("Carol", leiloeiro.retornarMaiorLance().getUsuario().getNome());
    }

    @Test
    public void testarRetornoDoMaiorLance(){
        Assertions.assertSame(lances.get(lances.size()-1),  leiloeiro.retornarMaiorLance());
    }

    @Test
    public void testarRetornoDoNomeNaoPreenchido(){
        Assertions.assertFalse(leiloeiro.retornarMaiorLance().getUsuario().getNome().length() > 0);
    }
}
