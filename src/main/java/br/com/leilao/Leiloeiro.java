package br.com.leilao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Leiloeiro {
    private String nome;

    private List<Lance> leilao;

    public Leiloeiro(String nome, List<Lance> leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Leiloeiro() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Lance> getLeilao() {
        return leilao;
    }

    public void setLeilao(List<Lance> leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        return getLeilao().get(getLeilao().size() -1);
    }
}
