package br.com.leilao;

public class Lance {
    private double valorDoLance;
    private Usuario usuario;

    public Lance(double valorDoLance, Usuario usuario) {
        this.valorDoLance = valorDoLance;
        this.usuario = usuario;
    }

    public Lance(double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }

    public Lance() {
    }

    public double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
