package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao() {
        lances = new ArrayList<>();
    }


    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }


    public Lance adicionarNovoLance(Lance lanceTeste) {
        try {
            Lance lanceObjeto = validarLance(lanceTeste);

            this.lances.add(lanceObjeto);
            return lanceObjeto;
        }
        catch (RuntimeException excecao){
            throw excecao;
        }
    }

    public Lance validarLance(Lance lance) {
        for(Lance lanceObjeto : this.getLances()){
            if(lance.getValorDoLance() < lanceObjeto.getValorDoLance()){
                throw new RuntimeException("Já existe um valor maior de lance");
            }
        }
        return lance;
    }

    public boolean verificarSeJaExisteLance(Lance lance){
        for(Lance lanceObjeto : this.getLances()){
            if(lance.getValorDoLance() == lanceObjeto.getValorDoLance()){
                return true;
            }
        }
        return false;
    }
}
